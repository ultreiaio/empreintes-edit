package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteAlbum;
import io.ultreia.empreintes.edit.model.SitePhoto;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SiteAlbumStorageTest {

    @Test
    public void readFromPath() throws IOException {

        Path albumPath = Paths.get(new File("").getAbsolutePath())
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("without_config")
                .resolve("photographe")
                .resolve("malte");

        SiteAlbum siteAlbum = SiteAlbumStorage.readFromPath(albumPath);
        assertAlbumPhotographe(siteAlbum, albumPath);

    }

    static void assertAlbumPhotographe(SiteAlbum siteAlbum, Path albumPath) {
        Assert.assertNotNull(siteAlbum);
        Assert.assertNotNull(siteAlbum.getPath());
        Assert.assertEquals(albumPath, siteAlbum.getPath());
//        Assert.assertEquals("Malte", siteAlbum.getName());
        Assert.assertEquals(1, siteAlbum.getItems().size());

        SitePhoto sitePhoto = siteAlbum.getItems().get(0);
        SitePageStorageTest.assertPhoto(sitePhoto, albumPath.resolve("01.jpg"), null, 2562, 2028);

    }
    @Test
    public void readFromConfig() {

        Path albumPath = Paths.get(new File("").getAbsolutePath())
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("with_config")
                .resolve("photographe")
                .resolve("malte");

        SiteAlbum siteAlbum = SiteAlbumStorage.readFromConfig(albumPath);
        Assert.assertNotNull(siteAlbum);
        Assert.assertNotNull(siteAlbum.getPath());
        Assert.assertEquals(albumPath, siteAlbum.getPath());
        Assert.assertEquals("Malte", siteAlbum.getName());
        Assert.assertEquals(1, siteAlbum.getItems().size());

        SitePhoto sitePhoto = siteAlbum.getItems().get(0);
        SitePageStorageTest.assertPhoto(sitePhoto, albumPath.resolve("01.jpg"), "Malte 01", 800, 600);

    }
}
