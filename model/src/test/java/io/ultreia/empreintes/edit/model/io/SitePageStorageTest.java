package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteAlbum;
import io.ultreia.empreintes.edit.model.SiteNode;
import io.ultreia.empreintes.edit.model.SitePage;
import io.ultreia.empreintes.edit.model.SitePageLink;
import io.ultreia.empreintes.edit.model.SitePhoto;
import io.ultreia.empreintes.edit.tool.DeleteConfigFilesTool;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SitePageStorageTest {

    static void assertPhoto(SitePhoto sitePhoto, Path path, String name, int width, int height) {
        Assert.assertNotNull(sitePhoto);
        Assert.assertNotNull(sitePhoto.getPath());
        Assert.assertEquals(path, sitePhoto.getPath());
        Assert.assertEquals(name, sitePhoto.getName());
        Assert.assertEquals(new Dimension(width, height), sitePhoto.getDimension());
    }

    static void assertPageLink(SitePageLink sitePageLink, Path path, String name, int x1, int y1, int x2, int y2) {
        assertPageLink(sitePageLink, path, name);
        Assert.assertEquals(new Point(x1, y1), sitePageLink.getTopLeft());
        Assert.assertEquals(new Point(x2, y2), sitePageLink.getBottomRight());
    }

    static void assertPageLink(SitePageLink sitePageLink, Path path, String name) {
        Assert.assertNotNull(sitePageLink);
        Assert.assertNotNull(sitePageLink.getTarget());
        Assert.assertNotNull(sitePageLink.getTarget().getPath());
        Assert.assertEquals(path, sitePageLink.getTarget().getPath());
        Assert.assertEquals(name, sitePageLink.getName());
    }

    @Test
    public void readRealFromPath() throws IOException {

        Path pagePath = Paths.get("/home/tc/Bureau/dad/new_site");
        Assume.assumeTrue(String.format("Path: %s does not exist", pagePath), Files.exists(pagePath));

        SitePage sitePage = SitePageStorage.readFromPath(pagePath);
        Assert.assertNotNull(sitePage);
        Assert.assertNotNull(sitePage.getPath());
        Assert.assertEquals(pagePath, sitePage.getPath());
        Assert.assertEquals(3, sitePage.getItems().size());

        SitePhoto sitePhoto = sitePage.getPage();
        assertPhoto(sitePhoto, pagePath.resolve("presentation_accueil.jpg"), null, 2644, 1624);

        assertPageLink(sitePage.getItems().get(0), pagePath.resolve("contact"), null);
        assertPageLink(sitePage.getItems().get(1), pagePath.resolve("livres"), null);
        assertPageLink(sitePage.getItems().get(2), pagePath.resolve("plasticien"), null);

    }

    @Test
    public void readAllFromPath() throws IOException {

        Path pagePath = getWithoutConfigRoot();

        SitePage sitePage = SitePageStorage.readFromPath(pagePath);
        Assert.assertNotNull(sitePage);
        Assert.assertNotNull(sitePage.getPath());
        Assert.assertEquals(pagePath, sitePage.getPath());
        Assert.assertEquals(2, sitePage.getItems().size());

        SitePhoto sitePhoto = sitePage.getPage();
        assertPhoto(sitePhoto, pagePath.resolve("presentation_accueil.jpg"), null, 3543, 2362);

        assertPageLink(sitePage.getItems().get(0), pagePath.resolve("photographe"), null);
        assertPageLink(sitePage.getItems().get(1), pagePath.resolve("plasticien"), null);

    }

    public static Path getWithoutConfigRoot() {
        return Paths.get(new File("").getAbsolutePath())
                    .resolve("src")
                    .resolve("test")
                    .resolve("resources")
                    .resolve("without_config");
    }

    @Test
    public void readAllFromConfig() {

        Path pagePath = Paths.get(new File("").getAbsolutePath())
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("with_config");

        SitePage sitePage = SitePageStorage.readFromConfig(pagePath);
        Assert.assertNotNull(sitePage);
        Assert.assertNotNull(sitePage.getPath());
        Assert.assertEquals(pagePath, sitePage.getPath());
        Assert.assertEquals(2, sitePage.getItems().size());

        SitePhoto sitePhoto = sitePage.getPage();
        assertPhoto(sitePhoto, pagePath.resolve("presentation_accueil.jpg"), "Accueil", 500, 300);

        assertPageLink(sitePage.getItems().get(0), pagePath.resolve("photographe"), "Photographe", 0, 0, 100, 100);
        assertPageLink(sitePage.getItems().get(1), pagePath.resolve("plasticien"), "Plasticien", 100, 100, 200, 200);

    }

    @Test
    public void readPhotographeFromPath() throws IOException {

        Path pagePath = Paths.get(new File("").getAbsolutePath())
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("without_config")
                .resolve("photographe");

        SitePage sitePage = SitePageStorage.readFromPath(pagePath);
        Assert.assertNotNull(sitePage);
        Assert.assertNotNull(sitePage.getPath());
        Assert.assertEquals(pagePath, sitePage.getPath());
        Assert.assertEquals(1, sitePage.getItems().size());

        SitePhoto sitePhoto = sitePage.getPage();
        assertPhoto(sitePhoto, pagePath.resolve("presentation_photographe.jpg"), null, 3072, 2304);

        SitePageLink sitePageLink = sitePage.getItems().get(0);
        assertPageLink(sitePageLink, pagePath.resolve("malte"), null);
        SiteNode target = sitePageLink.getTarget();
        Assert.assertNotNull(target);
        Assert.assertTrue(target instanceof SiteAlbum);
        SiteAlbum siteAlbum = (SiteAlbum) target;
        SiteAlbumStorageTest.assertAlbumPhotographe(siteAlbum, siteAlbum.getPath());
    }

    @Test
    public void readPhotographeFromConfig() {

        Path pagePath = Paths.get(new File("").getAbsolutePath())
                .resolve("src")
                .resolve("test")
                .resolve("resources")
                .resolve("with_config")
                .resolve("photographe");

        SitePage sitePage = SitePageStorage.readFromConfig(pagePath);
        Assert.assertNotNull(sitePage);
        Assert.assertNotNull(sitePage.getPath());
        Assert.assertEquals(pagePath, sitePage.getPath());
        Assert.assertEquals(1, sitePage.getItems().size());

        SitePhoto sitePhoto = sitePage.getPage();
        assertPhoto(sitePhoto, pagePath.resolve("presentation_photographe.jpg"), "Photographe", 900, 700);

        SitePageLink sitePageLink = sitePage.getItems().get(0);
        assertPageLink(sitePageLink, pagePath.resolve("malte"), "Malte", 0, 0, 100, 100);

    }

    @Test
    public void writeWithoutConfig() throws IOException {

        Path path = SitePageStorageTest.getWithoutConfigRoot();
        Assume.assumeTrue(String.format("Path: %s does not exist", path), Files.exists(path));
        DeleteConfigFilesTool.main(path.toString());

        SitePage sitePage = SitePageStorage.readFromPath(path);
        SitePageStorage.write(sitePage);

        DeleteConfigFilesTool.main(path.toString());

    }

    @Test
    @Ignore
    public void writeRealSite() throws IOException {

        Path path = Paths.get("/home/tc/Bureau/dad/new_site");
        Assume.assumeTrue(String.format("Path: %s does not exist", path), Files.exists(path));
        DeleteConfigFilesTool.main(path.toString());

        SitePage sitePage = SitePageStorage.readFromPath(path);
        SitePageStorage.write(sitePage);

//        DeleteConfigFilesTool.main(path.toString());
    }

}
