package io.ultreia.empreintes.edit.tool;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteAlbum;
import io.ultreia.empreintes.edit.model.SitePage;
import io.ultreia.empreintes.edit.model.SitePageLink;
import io.ultreia.empreintes.edit.model.io.SiteAlbumStorage;
import io.ultreia.empreintes.edit.model.io.SitePageStorage;
import io.ultreia.empreintes.edit.model.io.SitePageStorageTest;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created on 06/04/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class GenerateMissingConfigFilesToolTest {

    @Test
    public void testInline() throws IOException {

        Path path = SitePageStorageTest.getWithoutConfigRoot();
        Assume.assumeTrue(String.format("Path: %s does not exist", path), Files.exists(path));
        DeleteConfigFilesTool.main(path.toString());

        GenerateMissingConfigFilesTool.main(path.toString());

        SitePage sitePage = SitePageStorage.readFromConfig(path);

        Assert.assertTrue(Files.exists(SitePageStorage.getConfigPath(sitePage.getPath())));
        List<SitePageLink> items = sitePage.getItems();
        Assert.assertEquals(2, items.size());

        {
            SitePageLink sitePageLink = items.get(0);
            SitePage target = (SitePage) sitePageLink.getTarget();
            Assert.assertTrue(Files.exists(SitePageStorage.getConfigPath(target.getPath())));
            Assert.assertEquals(1, target.getItems().size());
            Assert.assertTrue(Files.exists(SiteAlbumStorage.getConfigPath(target.getItems().get(0).getTarget().getPath())));
        }
        {
            SitePageLink sitePageLink = items.get(1);
            SiteAlbum target = (SiteAlbum) sitePageLink.getTarget();
            Assert.assertTrue(Files.exists(SiteAlbumStorage.getConfigPath(target.getPath())));
        }

        DeleteConfigFilesTool.main(path.toString());

        Assert.assertFalse(Files.exists(SitePageStorage.getConfigPath(sitePage.getPath())));

        {
            SitePageLink sitePageLink = items.get(0);
            SitePage target = (SitePage) sitePageLink.getTarget();
            Assert.assertFalse(Files.exists(SitePageStorage.getConfigPath(target.getPath())));
            Assert.assertEquals(1, target.getItems().size());
            Assert.assertFalse(Files.exists(SiteAlbumStorage.getConfigPath(target.getItems().get(0).getTarget().getPath())));
        }
        {
            SitePageLink sitePageLink = items.get(1);
            SiteAlbum target = (SiteAlbum) sitePageLink.getTarget();
            Assert.assertFalse(Files.exists(SiteAlbumStorage.getConfigPath(target.getPath())));
        }
    }



    @Test
    public void generateOnRealSite() throws IOException {

        Path path = Paths.get("/home/tc/Bureau/dad/new_site");
        Assume.assumeTrue(String.format("Path: %s does not exist", path), Files.exists(path));

//        DeleteConfigFilesTool.main(path.toString());
        GenerateMissingConfigFilesTool.main(path.toString());
    }
}
