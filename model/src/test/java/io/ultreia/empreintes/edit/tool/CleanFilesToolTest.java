package io.ultreia.empreintes.edit.tool;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assume;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created on 05/04/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class CleanFilesToolTest {


    @Test
    public void testSite() throws IOException {

        Path path = Paths.get("/home/tc/Bureau/dad/new_site");
        Assume.assumeTrue(String.format("Path: %s does not exist", path), Files.exists(path));
        CleanFilesTool.main(path.toString());
    }
}
