package io.ultreia.empreintes.edit.model;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 27/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SitePhoto extends SiteElement {

    private static final Logger log = LogManager.getLogger(SitePhoto.class);

    private String name;
    private Dimension dimension;

    public Dimension computeDimension() {
        Path path = getPath();
        if (Files.exists(path)) {
            log.info(String.format("Loading photo %s", path));
            try {
                BufferedImage image = ImageIO.read(path.toFile());
                return new Dimension(image.getWidth(), image.getHeight());
            } catch (IOException e) {
                throw new IllegalStateException("Can't readFromConfig image at: " + path, e);
            }
        }
        return null;
    }

    public Dimension getDimension() {
        if (dimension == null && getPath() != null) {
            dimension = computeDimension();
        }
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
