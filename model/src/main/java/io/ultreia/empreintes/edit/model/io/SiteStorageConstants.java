package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public interface SiteStorageConstants {

    String PAGE_CONFIG_FILENAME = "page.config";

    String ALBUM_CONFIG_FILENAME = "album.config";
    String ALBUM_NAME = "album_name";

    String PHOTO_SECTION_PREFIX = "photo ";
    String PHOTO_NAME = "photo_name";
    String PHOTO_DIMENSION = "photo_dimension";

    String LINK_SECTION_PREFIX = "link ";
    String LINK_NAME = "link_name";
    String LINK_COORDINATE = "link_coordinate";

    static boolean isPhotoSection(String section) {
        return section != null && section.startsWith(PHOTO_SECTION_PREFIX);
    }

    static boolean isLinkSection(String section) {
        return section != null && section.startsWith(LINK_SECTION_PREFIX);
    }

    static INIConfiguration readConfiguration(Path configurationPath) {
        INIConfiguration iniConfiguration = new INIConfiguration();
        try (Reader reader = new BufferedReader(new InputStreamReader(configurationPath.toUri().toURL().openStream(), StandardCharsets.UTF_8))) {
            iniConfiguration.read(reader);
        } catch (ConfigurationException | IOException e) {
            throw new RuntimeException("Can't read configuration from: " + configurationPath, e);
        }
        return iniConfiguration;
    }

    static boolean isImage(Path path) {
        String name = path.toFile().getName();
        return !Files.isDirectory(path) && (name.endsWith(".jpg") || name.endsWith(".JPG"));
    }

    static boolean isPhoto(Path path) {
        return isImage(path) && !path.toFile().getName().startsWith("presentation");
    }

    static boolean isPresentation(Path path) {
        return isImage(path) && path.toFile().getName().startsWith("presentation");
    }

    static List<Path> getDirectories(Path path) throws IOException {
        return StreamSupport.stream(Files.newDirectoryStream(Objects.requireNonNull(path), Files::isDirectory).spliterator(), false).sorted(Comparator.comparing(p->p.toFile().getName())).collect(Collectors.toList());
    }

}
