package io.ultreia.empreintes.edit.tool;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.io.SiteAlbumStorage;
import io.ultreia.empreintes.edit.model.io.SitePageStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created on 05/04/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class DeleteConfigFilesTool {

    private static final Logger log = LogManager.getLogger(DeleteConfigFilesTool.class);

    public static void main(String... args) throws IOException {
        if (args.length != 1) {
            throw new IllegalStateException("Need exactly one argument: directory to clean");
        }

        Path path = Paths.get(new File(args[0]).getAbsolutePath());

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                if (SiteAlbumStorage.isSiteAlbumFromConfig(dir)) {
                    Path configPath = SiteAlbumStorage.getConfigPath(dir);
                    log.info(String.format("Delete config: %s", configPath));
                    Files.delete(configPath);
                }
                if (SitePageStorage.isSitePageFromConfig(dir)) {
                    Path configPath = SitePageStorage.getConfigPath(dir);
                    log.info(String.format("Delete config: %s", configPath));
                    Files.delete(configPath);
                }
                return FileVisitResult.CONTINUE;
            }

        });
    }

}
