package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteNode;
import io.ultreia.empreintes.edit.model.SitePage;
import io.ultreia.empreintes.edit.model.SitePageLink;
import io.ultreia.empreintes.edit.model.SitePhoto;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SitePageStorage implements SiteStorageConstants {

    private static final Logger log = LogManager.getLogger(SitePageStorage.class);

    public static void write(SitePage sitePage) throws IOException {
        log.debug(String.format("Writing %s", sitePage.getPath()));

        INIConfiguration iniConfiguration = new INIConfiguration();
        SitePhoto sitePhoto = sitePage.getPage();
        if (sitePhoto != null) {
            SitePhotoStorage.write(iniConfiguration, sitePhoto);
        }
        for (SitePageLink sitePageLink : sitePage.getItems()) {
            write(iniConfiguration, sitePageLink);
        }

        Path configPath = getConfigPath(sitePage.getPath());
        log.info(String.format("Writing to %s", configPath));
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(configPath)) {
            iniConfiguration.write(bufferedWriter);
        } catch (ConfigurationException e) {
            throw new IOException("Can't write init file: " + configPath, e);
        }

        for (SitePageLink sitePageLink : sitePage.getItems()) {
            SiteStorage.write(sitePageLink.getTarget());
        }
    }

    public static SitePage readFromPath(Path path) throws IOException {
        SitePage result = new SitePage();
        log.info(String.format("Reading %s", path));
        result.setPath(Objects.requireNonNull(path));
        Optional<Path> optionalPresentationPath = getPresentationFromPath(path);
        if (optionalPresentationPath.isPresent()) {
            SitePhoto sitePhoto = SitePhotoStorage.readFromPath(optionalPresentationPath.get());
            result.setPage(sitePhoto);
        }
        for (Path directory : SiteStorageConstants.getDirectories(path)) {
            SiteNode siteNode = SiteStorage.readFromPath(directory);
            SitePageLink sitePageLink = new SitePageLink();
            sitePageLink.setTarget(siteNode);
//            sitePageLink.setName(directory.toFile().getName());
            result.addItem(sitePageLink);
        }

        return result;
    }

    public static SitePage readFromConfig(Path path) {
        SitePage result = new SitePage();
        result.setPath(Objects.requireNonNull(path));
        Path configurationPath = path.resolve(PAGE_CONFIG_FILENAME);
        if (Files.exists(configurationPath)) {
            log.info("Loading page from: " + configurationPath);
            INIConfiguration iniConfiguration = SiteStorageConstants.readConfiguration(configurationPath);
            readFromConfig(result, iniConfiguration);
        }
        return result;
    }

    private static void write(INIConfiguration iniConfiguration, SitePageLink sitePageLink) throws IOException {
        SubnodeConfiguration section = iniConfiguration.getSection(LINK_SECTION_PREFIX + sitePageLink.getTarget().getPath().toFile().getName());
        String sitePageLinkName = sitePageLink.getName();
        section.addProperty(LINK_NAME, StringUtils.isEmpty(sitePageLinkName) ? "" : sitePageLinkName.trim());
        Point topLeft = sitePageLink.getTopLeft();
        Point bottomRight = sitePageLink.getBottomRight();
        if (topLeft != null && bottomRight != null) {
            section.addProperty(LINK_COORDINATE, String.format("%d,%d,%d,%d", (int) topLeft.getX(), (int) topLeft.getY(), (int) bottomRight.getX(), (int) bottomRight.getY()));
        } else {
            section.addProperty(LINK_COORDINATE, "");
        }
    }

    private static Optional<Path> getPresentationFromPath(Path path) throws IOException {
        Iterator<Path> iterator = Files.newDirectoryStream(Objects.requireNonNull(path), SiteStorageConstants::isPresentation).iterator();
        return Optional.ofNullable(iterator.hasNext() ? iterator.next() : null);
    }

    private static void readFromConfig(SitePage result, INIConfiguration iniConfiguration) {
        Path path = result.getPath();
        for (String section : iniConfiguration.getSections()) {
            if (section == null) {
                continue;
            }
            if (SiteStorageConstants.isPhotoSection(section)) {
                SitePhoto sitePhoto = SitePhotoStorage.readFromConfig(path, iniConfiguration, section);
                result.setPage(sitePhoto);
                continue;
            }
            if (SiteStorageConstants.isLinkSection(section)) {
                SubnodeConfiguration linkSection = iniConfiguration.getSection(section);
                String linkFilename = section.substring(LINK_SECTION_PREFIX.length());
                SitePageLink sitePageLink = new SitePageLink();
                readFromConfig(sitePageLink, path.resolve(linkFilename), linkSection);
                result.addItem(sitePageLink);
            }
        }
    }

    private static void readFromConfig(SitePageLink result, Path path, SubnodeConfiguration linkSection) {
        String linkName = linkSection.getString(LINK_NAME);
        result.setName(StringUtils.isEmpty(linkName) ? null : linkName);
        String coordinate = linkSection.getString(LINK_COORDINATE);
        if (StringUtils.isNotEmpty(coordinate)) {
            String[] split = coordinate.trim().split(",");
            result.setTopLeft(new Point(Integer.valueOf(split[0]), Integer.valueOf(split[1])));
            result.setBottomRight(new Point(Integer.valueOf(split[2]), Integer.valueOf(split[3])));
        }
        SiteNode siteNode = SiteStorage.readFromConfig(path);
        result.setTarget(siteNode);
    }

    public static Path getConfigPath(Path path) {
        return path.resolve(PAGE_CONFIG_FILENAME);
    }

    public static boolean isSitePageFromConfig(Path path) {
        return Files.exists(getConfigPath(path));
    }

    public static boolean isSitePageFromPath(Path path) throws IOException {
        return !SiteStorageConstants.getDirectories(path).isEmpty();
    }
}
