package io.ultreia.empreintes.edit.tool;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.io.SiteStorageConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Objects;

/**
 * Created on 05/04/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class CleanFilesTool {

    private static final Logger log = LogManager.getLogger(CleanFilesTool.class);

    public static void main(String... args) throws IOException {
        if (args.length != 1) {
            throw new IllegalStateException("Need exactly one argument: directory to clean");
        }

        Path path = Paths.get(new File(args[0]).getAbsolutePath());

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (SiteStorageConstants.isImage(file)) {
                    cleanFile(file);
                }
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

        });
    }

    private static void cleanFile(Path file) throws IOException {
        log.debug(String.format("Clean image: %s", file));
        String originalName = file.toFile().getName();
        String name = originalName;
        int index = name.indexOf('(');
        if (index > -1) {
            name = name.substring(0, index).trim() + ".jpg";
        }
        if (name.endsWith(" .jpg")) {
            name = name.substring(0, name.length() - 5) + ".jpg";
        }
        if (!name.endsWith(".jpg")) {
            name = name.substring(0, name.length() - 3) + ".jpg";
        }
        if (name.endsWith(".JPG")) {
            name = name.substring(0, name.length() - 4) + ".jpg";
        }
        if (name.contains("_site_presentation_plasticien_")) {
            name = StringUtils.remove(name, "site_presentation_plasticien");
        }
        if (name.contains("_site_photographe_")) {
            name = StringUtils.remove(name, "site_photographe");
        }
        if (name.contains("_site_plasticien_")) {
            name = StringUtils.remove(name, "site_");
        }
        if (name.contains("_photographe_nprmandie_")) {
            name = StringUtils.replace(name, "_photographe_nprmandie_","_normandie_");
        }
        if (name.contains("_maroc_fantasiamaroc_fes_fantasia_2010")) {
            name = StringUtils.replace(name, "_maroc_fantasiamaroc_fes_fantasia_2010","_maroc_fantasia_fes_2010");
        }
        if (name.contains("_europe_republiquetcheque_prague")) {
            name = StringUtils.replace(name, "_europe_republiquetcheque_prague","_prague");
        }
        if (name.contains("_oleron1_france_charente maritime_iledoleron")) {
            name = StringUtils.replace(name, "_oleron1_france_charente maritime_iledoleron","_oleron");
        }
        if (name.contains("_photographe_musiques_")) {
            name = StringUtils.replace(name, "_photographe_musiques_","_musiques_");
        }
        if (name.contains("_photographie_musiques_")) {
            name = StringUtils.replace(name, "_photographie_musiques_","_musiques_");
        }
        name = name.replaceAll("__","_");

        name = name.trim();
        if (!Objects.equals(name, originalName)) {
            Path target = file.getParent().resolve(name);
            Files.move(file, target);
            log.info(String.format("Move image: %s to %s", originalName, name));
            if (SiteStorageConstants.isPresentation(file)) {
                Path sourcePsd = file.getParent().resolve(originalName.substring(0, originalName.length() - 4) + ".psd");
                if (Files.exists(sourcePsd)) {
                    Path targetPsd = file.getParent().resolve(name.substring(0, name.length() - 4) + ".psd");
                    Files.move(sourcePsd, targetPsd, StandardCopyOption.ATOMIC_MOVE);
                    log.info(String.format("Move psd: %s to %s", sourcePsd.toFile().getName(), targetPsd.toFile().getName()));
                }
            }
        }
    }
}
