package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteAlbum;
import io.ultreia.empreintes.edit.model.SitePhoto;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SiteAlbumStorage implements SiteStorageConstants {

    private static final Logger log = LogManager.getLogger(SiteAlbumStorage.class);

    public static void write(SiteAlbum siteAlbum) throws IOException {
        log.debug(String.format("Writing %s", siteAlbum.getPath()));

        INIConfiguration iniConfiguration = new INIConfiguration();
        String name = siteAlbum.getName();
        iniConfiguration.setProperty(ALBUM_NAME, StringUtils.isEmpty(name) ? "" : name.trim());
        siteAlbum.getItems().parallelStream().forEach(SitePhoto::getDimension);
        siteAlbum.getItems().forEach(l -> SitePhotoStorage.write(iniConfiguration, l));

        Path configPath = getConfigPath(siteAlbum.getPath());
        log.info(String.format("Writing to %s", configPath));
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(configPath)) {
            iniConfiguration.write(bufferedWriter);
        } catch (ConfigurationException e) {
            throw new IOException("Can't write init file: " + configPath, e);
        }
    }

    public static SiteAlbum readFromPath(Path path) throws IOException {
        log.info(String.format("Reading %s", path));
        SiteAlbum result = new SiteAlbum();
        result.setPath(Objects.requireNonNull(path));
        List<Path> photos = SitePhotoStorage.getPhotosFromPath(path);
        for (Path photoPath : photos) {
            SitePhoto sitePhoto = SitePhotoStorage.readFromPath(photoPath);
            result.addItem(sitePhoto);
        }
        return result;
    }

    public static SiteAlbum readFromConfig(Path path) {
        SiteAlbum result = new SiteAlbum();
        result.setPath(Objects.requireNonNull(path));
        Path configurationPath = path.resolve(ALBUM_CONFIG_FILENAME);
        if (Files.exists(configurationPath)) {
            log.info("Loading album from: " + configurationPath);
            INIConfiguration iniConfiguration = SiteStorageConstants.readConfiguration(configurationPath);
            readFromConfig(result, iniConfiguration);
        }
        return result;
    }

    private static void readFromConfig(SiteAlbum result, INIConfiguration iniConfiguration) {
        String albumName = iniConfiguration.getString(ALBUM_NAME);
        result.setName(StringUtils.isEmpty(albumName) ? null : albumName.trim());
        Path path = result.getPath();
        for (String section : iniConfiguration.getSections()) {
            if (SiteStorageConstants.isPhotoSection(section)) {
                SitePhoto sitePhoto = SitePhotoStorage.readFromConfig(path, iniConfiguration, section);
                result.addItem(sitePhoto);
            }
        }
    }

    public static Path getConfigPath(Path path) {
        return path.resolve(ALBUM_CONFIG_FILENAME);
    }

    public static boolean isSiteAlbumFromConfig(Path path) {
        return Files.exists(getConfigPath(path));
    }

    public static boolean isSiteAlbumFromPath(Path path) throws IOException {
        return SiteStorageConstants.getDirectories(path).isEmpty();
    }
}
