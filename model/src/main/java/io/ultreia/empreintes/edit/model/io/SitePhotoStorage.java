package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SitePhoto;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Dimension;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SitePhotoStorage implements SiteStorageConstants {

    private static final Logger log = LogManager.getLogger(SitePhotoStorage.class);

    static List<Path> getPhotosFromPath(Path path) throws IOException {
        return StreamSupport.stream(Files.newDirectoryStream(Objects.requireNonNull(path), SiteStorageConstants::isPhoto).spliterator(), false).sorted(Comparator.comparing(p -> p.toFile().getName())).collect(Collectors.toList());
    }

    static SitePhoto readFromConfig(Path path, INIConfiguration configuration, String section) {
        SubnodeConfiguration photoSection = configuration.getSection(section);
        String photoFilename = section.substring(SitePhotoStorage.PHOTO_SECTION_PREFIX.length()).trim() + ".jpg";

        SitePhoto result = new SitePhoto();
        result.setPath(path.resolve(photoFilename));

        String photoName = photoSection.getString(PHOTO_NAME);
        result.setName(StringUtils.isEmpty(photoName) ? null : photoName.trim());
        String dimension = photoSection.getString(PHOTO_DIMENSION);
        if (StringUtils.isNotEmpty(dimension)) {
            String[] split = dimension.split(",");
            result.setDimension(new Dimension(Integer.valueOf(split[0]), Integer.valueOf(split[1])));
        }
        return result;
    }

    static SitePhoto readFromPath(Path photoPath) {
        log.info(String.format("Reading %s", photoPath));
        SitePhoto result = new SitePhoto();
        result.setPath(photoPath);
        return result;
    }

    static void write(INIConfiguration iniConfiguration, SitePhoto sitePhoto) {
        SubnodeConfiguration section = iniConfiguration.getSection(PHOTO_SECTION_PREFIX + StringUtils.removeEnd(sitePhoto.getPath().toFile().getName(), ".jpg"));
        String sitePhotoName = sitePhoto.getName();
        section.addProperty(PHOTO_NAME, StringUtils.isEmpty(sitePhotoName) ? "" : sitePhotoName.trim());
        Dimension dimension = sitePhoto.getDimension();
        section.addProperty(PHOTO_DIMENSION, String.format("%d,%d", (int) dimension.getWidth(), (int) dimension.getHeight()));
    }
}
