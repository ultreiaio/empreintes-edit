package io.ultreia.empreintes.edit.tool;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteAlbum;
import io.ultreia.empreintes.edit.model.SitePage;
import io.ultreia.empreintes.edit.model.SitePageLink;
import io.ultreia.empreintes.edit.model.SitePhoto;
import io.ultreia.empreintes.edit.model.io.SiteAlbumStorage;
import io.ultreia.empreintes.edit.model.io.SitePageStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

/**
 * Created on 06/04/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class GenerateMissingConfigFilesTool {

    private static final Logger log = LogManager.getLogger(GenerateMissingConfigFilesTool.class);

    public static void main(String... args) throws IOException {
        if (args.length != 1) {
            throw new IllegalStateException("Need exactly one argument: directory to clean");
        }

        Path path = Paths.get(new File(args[0]).getAbsolutePath());

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                if (SiteAlbumStorage.isSiteAlbumFromConfig(dir)) {
                    updateAlbum(dir);
                } else if (SitePageStorage.isSitePageFromConfig(dir)) {
                    updatePage(dir);
                } else if (SiteAlbumStorage.isSiteAlbumFromPath(dir)) {
                    generateAlbum(dir);
                } else if (SitePageStorage.isSitePageFromPath(dir)) {
                    generatePage(dir);
                }
                return FileVisitResult.CONTINUE;
            }

        });
    }

    private static void updateAlbum(Path dir) throws IOException {
        log.info(String.format("Update album config for: %s", dir));
        SiteAlbum siteAlbumFromPath = SiteAlbumStorage.readFromPath(dir);
        SiteAlbum siteAlbumFromConfig = SiteAlbumStorage.readFromConfig(dir);
        List<SitePhoto> itemsFromPath = siteAlbumFromPath.getItems();
        List<SitePhoto> itemsFromConfig = siteAlbumFromConfig.getItems();
        itemsFromConfig.removeIf(i -> !itemsFromPath.contains(i));
        for (SitePhoto sitePhoto : itemsFromPath) {
            if (!itemsFromConfig.contains(sitePhoto)) {
                siteAlbumFromConfig.addItem(sitePhoto);
            }
        }
        siteAlbumFromConfig.sortItems();
        SiteAlbumStorage.write(siteAlbumFromConfig);
    }

    private static void updatePage(Path dir) throws IOException {
        log.info(String.format("Update page config for: %s", dir));
        SitePage sitePageFromPath = SitePageStorage.readFromPath(dir);
        SitePage sitePageFromConfig = SitePageStorage.readFromConfig(dir);
        List<SitePageLink> itemsFromPath = sitePageFromPath.getItems();
        List<SitePageLink> itemsFromConfig = sitePageFromConfig.getItems();
        itemsFromConfig.removeIf(i -> !itemsFromPath.contains(i));
        for (SitePageLink sitePageLink : itemsFromPath) {
            if (!itemsFromConfig.contains(sitePageLink)) {
                sitePageFromConfig.addItem(sitePageLink);
            }
        }
        sitePageFromConfig.sortItems();
        SitePageStorage.write(sitePageFromConfig);
    }

    private static void generateAlbum(Path dir) throws IOException {
        log.info(String.format("Generate album config for: %s", dir));
        SiteAlbum siteAlbum = SiteAlbumStorage.readFromPath(dir);
        SiteAlbumStorage.write(siteAlbum);
    }

    private static void generatePage(Path dir) throws IOException {
        log.info(String.format("Generate page config for: %s", dir));
        SitePage sitePage = SitePageStorage.readFromPath(dir);
        SitePageStorage.write(sitePage);
    }

}
