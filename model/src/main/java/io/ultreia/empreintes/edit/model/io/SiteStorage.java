package io.ultreia.empreintes.edit.model.io;

/*-
 * #%L
 * Empreintes-edit :: Model
 * %%
 * Copyright (C) 2019 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.empreintes.edit.model.SiteAlbum;
import io.ultreia.empreintes.edit.model.SiteNode;
import io.ultreia.empreintes.edit.model.SitePage;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created on 28/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class SiteStorage implements SiteStorageConstants {

    public static void write(SiteNode siteNode) throws IOException {
        if (siteNode instanceof SiteAlbum) {
            SiteAlbumStorage.write((SiteAlbum) siteNode);
        } else if (siteNode instanceof SitePage) {
            SitePageStorage.write((SitePage) siteNode);
        }
    }

    public static SiteNode readFromPath(Path path) throws IOException {
        if (SiteAlbumStorage.isSiteAlbumFromPath(path)) {
            return SiteAlbumStorage.readFromPath(path);
        }
        if (SitePageStorage.isSitePageFromPath(path)) {
            return SitePageStorage.readFromPath(path);
        }
        return null;
    }

    public static SiteNode readFromConfig(Path path) {
        if (SiteAlbumStorage.isSiteAlbumFromConfig(path)) {
            return SiteAlbumStorage.readFromConfig(path);
        }
        if (SitePageStorage.isSitePageFromConfig(path)) {
            return SitePageStorage.readFromConfig(path);
        }
        return null;
    }
}
